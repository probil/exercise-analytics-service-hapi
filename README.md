# Analytic Service Skeleton

> Playbuzz backend exercise


## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.


### Requirements
[Node.js](https://nodejs.org/en/) (>=8.x), npm version 5+ and [Git](https://git-scm.com/).

Optionally: MySQL 5.5+ (in case you want to use your local DB)


### Installing
Just clone the repo and install dependencies:
```bash
git clone https://bitbucket.org/probil/exercise-analytics-service-hapi.git
cd exercise-analytics-service-hapi
npm i
```


### Running the app
Start the API with default config
```bash
npm start  
```
Then open in browser:
```
http://localhost:3000/v1/      # API base url
http://localhost:3000/docs     # API docs (build with `hapi-swagger`)
```

## Running the tests

### Coding style tests

I use [`airbnb-base`](https://github.com/airbnb/javascript) style guide for javascript code. It's recommended to use this style-guide for all the code in this project.

```bash
# run the tests for the whole app
npm run lint
# run the tests for particular file
npx eslint src/path/to/file.js
```
You can integrate code style tests right to your IDE or editor. Just plugin for eslint integration and point it to the `.eslintrc.js` file.

### Functional, integration and unit tests
I use [`lab`](https://github.com/hapijs/lab) as test framework. It's fork of [`mocha`](https://mochajs.org/) and in most cases it have the same behaviour. You you have any issue with it just check the docs.
```bash
# run the tests for the whole app
npm run test
# run particular test file
npx lab test/functional/getPageViewByCountry.spec.js
```

## Directory structure

```
.
├── config    # stores environment-specific configuration files
├── helpers   # contains different helper functions
│   └── array  # helpers related to arrays
├── models   # contains Sequelize models
├── plugins  # contains hapi plugins
├── routes   # contains hapi routes
│   └── page-view  # contains hapi routes in page-view group
├── schemas  # joi-schemas (for object validation)
├── tasks    # useful tasks (e.g. sync db with model, etc) 
└── test     # tests
    ├── fixtures    # contains special data placeholder for tests
    ├── functional  # contains functional tests
    └── helpers     # helper functions for tests
```

## Built With

* [ES6+](https://github.com/lukehoban/es6features) - ECMAScript 6, also known as ECMAScript 2015, is the latest version of the ECMAScript standard
* [Hapi](https://hapijs.com/) - A rich framework for building applications and services
* [Joi](https://github.com/hapijs/joi) - Object schema description language and validator for JavaScript objects.
* [Hapi-swagger](https://github.com/glennjones/hapi-swagger) - A Swagger interface for HAPI
* [Boom](https://github.com/hapijs/boom) - HTTP-friendly error objects
* [Node-config](https://github.com/lorenwest/node-config) - Node.js Application Configuration
* [Lab](https://github.com/hapijs/lab) - Node test utility
* [Sequelize](https://github.com/sequelize/sequelize) - An easy-to-use multi SQL dialect ORM for Node.js
* [Sequelize-fixtures](https://github.com/domasx2/sequelize-fixtures) - Load data from json, yaml or js to sequelize (for testing purpose)
* [ESlint](https://eslint.org/) - A fully pluggable tool for identifying and reporting on patterns in JavaScript.
* [Sinon](http://sinonjs.org/) -Standalone test spies, stubs and mocks for JavaScript. 

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).