'use strict';

const Boom = require('boom');
const token = require('config').get('auth.token');

/**
 * Simple auth scheme implementation
 * @see https://hapijs.com/api#serverauthschemename-scheme
 */
const scheme = () => ({
  authenticate(request, reply) {
    const { req: { headers: { authorization } } } = request.raw;

    // TODO: Secure from timing attack (e.g. by using `cryptiles` from npm)
    // see https://www.wikiwand.com/en/Timing_attack
    if (!authorization || authorization !== token) {
      return reply(Boom.unauthorized());
    }

    return reply.continue({ credentials: { user: 'some-user' } });
  },
});

/**
 * Plugin registration implementation
 * @param {Object} server - Hapi server
 * @param {Object} options - Plugin options
 * @param {Function} next - Callback function to notify that plugin registration is done
 */
exports.register = (server, options, next) => {
  server.auth.scheme('customToken', scheme);
  next();
};

exports.register.attributes = {
  name: 'custom-token-auth-scheme',
  multiple: false,
};
