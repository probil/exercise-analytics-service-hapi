'use strict';

const HapiSwagger = require('hapi-swagger');
const config = require('config');

const apiConfig = config.get('api');
const docsConfig = config.get('docs');

const swaggerOptions = {
  info: {
    title: docsConfig.title,
    version: apiConfig.version,
  },
  schemes: ['http'],
  basePath: `/${apiConfig.version}/`,
  pathPrefixSize: 2,
  jsonEditor: true,
  documentationPath: docsConfig.documentationPath,
  swaggerUIPath: '/docs/swaggerui/',
  jsonPath: '/docs/swagger.json',
  securityDefinitions: {
    default: {
      type: 'apiKey',
      description: 'JWT token',
      name: 'Authorization',
      in: 'header',
    },
  },
  uiCompleteScript: `
    var lsKey = '__AUTH-TOKEN__';
    var $inputApiKey = $('#input_apiKey');
    
    // Recover accessToken from localStorage if present.
    var key = window.localStorage && window.localStorage.getItem(lsKey);
    if (key) {
      $inputApiKey.val(key);
      // update auth
      addApiKeyAuthorization();
    }
    
    $inputApiKey.change(function () {
      var key = $inputApiKey[0].value;
      if (key && key.trim() !== '') {
        // Save this token to localStorage if we can to make it persist on refresh.
        window.localStorage && window.localStorage.setItem(lsKey, key);
      }
      else {
        window.localStorage && window.localStorage.removeItem(lsKey);
      }
    });
  `,
  tags: [
    {
      name: 'page-view',
      description: 'Operations with page-views',
    },
  ],
};

module.exports = {
  register: HapiSwagger,
  options: swaggerOptions,
};
