'use strict';

const glob = require('globby');
const cast = require('../helpers/array/cast');
const registerFilesAsRoutes = require('../helpers/hapi/registerFilesAsRoutes');

/**
 * Special hapi-plugin to auto-register routes from given folder
 * @see https://hapijs.com/tutorials/plugins
 * @param server           - Hapi server instance
 * @param options          - Plugin options
 * @param [options.routes] - glob matching pattern (see `globby`)
 * @param next             - Callback function
 * @returns {Promise}
 */
exports.register = (server, options, next) => {
  const cwd = options.cwd || process.cwd();
  const routes = cast(options.routes || 'routes/**/*.js');

  return glob(routes)
    .then(registerFilesAsRoutes(server, require, cwd))
    .then(() => next())
    .catch((err) => {
      console.error(err);
      next(err);
    });
};

exports.register.attributes = {
  name: 'routes-loader',
  multiple: false,
};
