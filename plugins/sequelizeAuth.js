'use strict';

const { sequelize } = require('../models');

exports.register = (server, options, next) => (
  sequelize.authenticate()
    .then(() => {
      console.info('Database connection has been established successfully.');
      return next();
    })
    .catch((err) => {
      console.error('Unable to connect to the database:', err.message || err);
      return process.exit();
    })
);

exports.register.attributes = {
  name: 'sequelize-auth',
  multiple: false,
};
