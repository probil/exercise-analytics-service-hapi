'use strict';

const { sequelize } = require('../models');

sequelize
  .sync()
  .then(() => {
    console.info('Done!');
    process.exit();
  })
  .catch((err) => {
    console.error(err.message || err);
    process.exit(1);
  });
