'use strict';

const Joi = require('joi');

module.exports = Joi.object().label('BoomError').unknown().keys({
  statusCode: Joi.number().min(100).integer()
    .example(501)
    .description('Status code of the error'),
  error: Joi.string()
    .example('Not Implemented')
    .description('Error title'),
  message: Joi.string()
    .example('This endpoint is not implemented')
    .description('Error message'),
});
