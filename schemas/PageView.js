'use strict';

const Joi = require('joi');

module.exports = Joi.object().label('PageView').unknown().keys({
  id: Joi.number().integer()
    .example(11)
    .description('Unique id of the event'),
  timestamp: Joi.date().timestamp()
    .example('1511015518095')
    .description('Timestamp of the event in milliseconds'),
  browser: Joi.string()
    .example('Firefox')
    .description('Browser name'),
  country: Joi.string()
    .example('Ukraine')
    .description('Country of the target user'),
  'user-id': Joi.number().integer().positive()
    .example(121)
    .description('Unique ID of the user'),
  'page-id': Joi.number().integer().positive()
    .example(15)
    .description('Unique ID of the page'),
  'page-url': Joi.string().uri({ allowRelative: true })
    .example('http://example.com/some-page')
    .description('Url of the page where even occurred'),
  'page-referrer': Joi.string()
    .example('http://example.com/referrer')
    .description('Url of the linked resource'),
  'user-agent': Joi.string()
    .example('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36')
    .description('User-Agent of target device'),
  'screen-resolution': Joi.string().regex(/^\d{3,4}x\d{3,4}$/)
    .example('1280x1024')
    .description('Resolution of target device'),
  'user-IP': Joi.string().ip()
    .example('52.41.81.252')
    .description('User IP'),
});
