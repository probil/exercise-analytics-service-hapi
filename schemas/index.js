'use strict';

const BoomErrorSchema = require('./BoomError');
const PageViewSchema = require('./PageView');

/**
 * Re-export schemas for easier access using destruction (ES6+)
 */
module.exports = {
  BoomErrorSchema,
  PageViewSchema,
};
