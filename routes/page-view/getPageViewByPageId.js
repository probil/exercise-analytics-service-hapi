'use strict';

const config = require('config');
const Joi = require('joi');
const { PageViewSchema } = require('../../schemas');
const { Event } = require('../../models');

module.exports = {
  method: 'GET',
  path: `/${config.get('api.version')}/page-view/by-page-id/{pageId}`,
  config: {
    description: 'Returns page-views by Page ID',
    tags: ['api'],
    plugins: {
      'hapi-swagger': {
        responses: {
          200: { description: 'Success', schema: Joi.array().items(PageViewSchema) },
          401: { description: 'Unauthorized' },
          500: { description: 'InternalServerError' },
        },
      },
    },
    validate: {
      params: {
        pageId: Joi.number().integer().positive().example(121)
          .description('Unique ID of the page'),
      },
    },
  },
  handler({ params: { pageId } }, reply) {
    return reply(Event.findAll({ where: { 'page-id': pageId } }));
  },
};
