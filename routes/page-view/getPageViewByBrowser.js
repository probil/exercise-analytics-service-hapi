'use strict';

const config = require('config');
const Joi = require('joi');
const { PageViewSchema } = require('../../schemas');
const { Event } = require('../../models');

module.exports = {
  method: 'GET',
  path: `/${config.get('api.version')}/page-view/by-browser/{browser}`,
  config: {
    description: 'Returns page-views by Browser',
    tags: ['api'],
    plugins: {
      'hapi-swagger': {
        responses: {
          200: { description: 'Success', schema: Joi.array().items(PageViewSchema) },
          401: { description: 'Unauthorized' },
          500: { description: 'InternalServerError' },
        },
      },
    },
    validate: {
      params: {
        browser: Joi.string().example('Firefox')
          .description('Browser name'),
      },
    },
  },
  handler({ params: { browser } }, reply) {
    return reply(Event.findAll({ where: { browser } }));
  },
};
