'use strict';

const config = require('config');
const Joi = require('joi');
const { PageViewSchema } = require('../../schemas');
const { Event } = require('../../models');

module.exports = {
  method: 'GET',
  path: `/${config.get('api.version')}/page-view/by-country/{country}`,
  config: {
    description: 'Returns page-views by Country',
    tags: ['api'],
    plugins: {
      'hapi-swagger': {
        responses: {
          200: { description: 'Success', schema: Joi.array().items(PageViewSchema) },
          401: { description: 'Unauthorized' },
          500: { description: 'InternalServerError' },
        },
      },
    },
    validate: {
      params: {
        country: Joi.string().example('Ukraine')
          .description('Name of the country'),
      },
    },
  },
  handler({ params: { country } }, reply) {
    return reply(Event.findAll({ where: { country } }));
  },
};
