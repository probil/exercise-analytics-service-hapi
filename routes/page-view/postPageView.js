'use strict';

const config = require('config');
const Joi = require('joi');
const { PageViewSchema } = require('../../schemas');
const { Event } = require('../../models');

module.exports = {
  method: 'POST',
  path: `/${config.get('api.version')}/page-view`,
  config: {
    description: 'Collects page-view events',
    tags: ['api'],
    plugins: {
      'hapi-swagger': {
        payloadType: 'form',
        responses: {
          201: { description: 'Created', schema: PageViewSchema },
          400: { description: 'BadRequest' },
          401: { description: 'Unauthorized' },
          500: { description: 'InternalServerError' },
        },
      },
    },
    validate: {
      payload: Joi.object().options({ stripUnknown: true }).keys({
        timestamp: Joi.date().timestamp()
          .example('1511015518095')
          .description('Timestamp of the event in milliseconds'),
        'user-id': Joi.number().integer().positive()
          .example(121)
          .description('Unique ID of the user'),
        'page-id': Joi.number().integer().positive()
          .example(15)
          .description('Unique ID of the page'),
        'page-url': Joi.string().uri({ allowRelative: true })
          .example('http://example.com/some-page')
          .description('Url of the page where even occurred'),
        'page-referrer': Joi.string()
          .example('http://example.com/referrer')
          .description('Url of the linked resource'),
        'user-agent': Joi.string()
          .example('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36')
          .description('User-Agent of target device'),
        'screen-resolution': Joi.string().regex(/^\d{3,4}x\d{3,4}$/)
          .example('1280x1024')
          .description('Resolution of target device'),
        'user-IP': Joi.string().ip()
          .example('52.41.81.252')
          .description('User IP'),
      }),
    },
  },
  handler(req, reply) {
    return reply(Event.create(req.payload)).code(201);
  },
};
