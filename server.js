'use strict';

const Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');
const config = require('config');
const hapiSwagger = require('./plugins/hapiSwagger');
const routesLoader = require('./plugins/routesLoader');
const customTokenAuthScheme = require('./plugins/customTokenAuthScheme');
const sequelizeAuth = require('./plugins/sequelizeAuth');

const server = new Hapi.Server();

server.connection({
  host: config.get('api.host') || 'localhost',
  port: config.get('api.port') || 3000,
});

server
  .register([
    Inert,
    Vision,
    hapiSwagger,
    sequelizeAuth,
    customTokenAuthScheme,
    routesLoader,
  ])
  .then(async () => {
    // use `customToken` auth scheme by default
    server.auth.strategy('default', 'customToken', 'required');
    if (process.env.NODE_ENV === 'test') return;
    await server.start();
    console.info('Server running at:', server.info.uri);
    console.info('API docs running at:', `${server.info.uri}${config.get('docs.documentationPath')}`);
  });

module.exports = server;
