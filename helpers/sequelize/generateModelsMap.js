'use strict';

const cast = require('../array/cast');
/**
 * Convert array of models to the map of models
 * @param {Model[]|Object[]|Model|Object} models - Sequelize Model or Models
 * @returns {Object} Map
 */
module.exports = models => (
  cast(models || [])
    .reduce((acc, model) => {
      if (!model.name) return acc;
      acc[model.name] = model;
      return acc;
    }, {})
);
