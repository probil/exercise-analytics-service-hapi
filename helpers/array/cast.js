'use strict';

/**
 * Ensure a value is an array and wrap it if it is not an array
 * @param {Array|*} value - Data that need to be cast
 * @returns {Array} - Casted array
 */
module.exports = value => (Array.isArray(value) ? value : [value]);
