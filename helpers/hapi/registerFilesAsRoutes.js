'use strict';

const cast = require('../array/cast');
/**
 * Special function to register array of files on server routes
 * @param {Server} server - Hapi server instance
 * @param {Function} load - Function for loading files (e.g. require, proxyquire, etc)
 * @param {String} cwd - Base directory
 */
module.exports = (server, load, cwd) => (fileNames) => {
  cast(fileNames || [])
    .map(fileName => load(`${cwd}/${fileName}`))
    .filter(route => typeof route === 'object')
    .forEach(route => server.route(route));
};
