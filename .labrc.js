'use strict';

module.exports = {
  lint: true,
  verbose: true,
  assert: 'code',
  timeout: 10000
};
