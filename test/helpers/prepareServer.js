'use strict';

const serverPromise = require('../../server');
const models = require('../../models');
const fixtures = require('sequelize-fixtures');

/**
 * Special helper for tests
 * @param {String[]} [seeders] - Array of seeders
 * @param {Boolean} [forceSync] - Force sync flag
 * @returns {Promise.<Server>}
 */
module.exports = async ({ seeders, forceSync }) => {
  const server = await serverPromise;
  if (forceSync) {
    await models.sequelize.sync({ force: true });
  }
  await fixtures.loadFiles(seeders || [], models);
  return server;
};
