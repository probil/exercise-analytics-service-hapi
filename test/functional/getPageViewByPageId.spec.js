'use strict';

/* eslint-disable no-multi-assign */
const {
  describe, it, expect, before,
} = exports.lab = require('lab').script();
const prepareServer = require('../helpers/prepareServer');

let server;
const baseUrl = '/v1/page-view/by-page-id';
const method = 'GET';
const validHeaders = { authorization: 'SuperSecretToken' };
const invalidHeaders = { authorization: 'SomethingElse' };

describe('GET /page-view/by-page-id', () => {
  before(async () => {
    server = await prepareServer({ forceSync: true, seeders: ['test/fixtures/events.json'] });
  });
  it('Status code 401 | If auth token is missing', async () => {
    const url = `${baseUrl}/19`;
    const response = await server.inject({ url, method });
    expect(response.statusCode).to.equal(401);
  });
  it('Status code 401 | If auth token is wrong', async () => {
    const url = `${baseUrl}/19`;
    const response = await server.inject({ url, method, headers: invalidHeaders });
    expect(response.statusCode).to.equal(401);
  });
  it('Status code 200 | If auth token is right', async () => {
    const url = `${baseUrl}/19`;
    const response = await server.inject({ url, method, headers: validHeaders });
    expect(response.statusCode).to.equal(200);
  });
  it('Status code 200 | Should return array of events from specific page', async () => {
    const pageId = 19;
    const url = `${baseUrl}/${pageId}`;
    const response = await server.inject({ url, method, headers: validHeaders });
    expect(response.statusCode).to.equal(200);
    const payload = JSON.parse(response.payload);
    expect(payload).to.be.an.array();
    expect(payload).to.have.length(1);
    expect(payload[0]).to.be.an.object();
    expect(payload[0]).to.include([
      'id',
      'timestamp',
      'browser',
      'country',
      'user-id',
      'page-id',
      'page-url',
      'page-referrer',
      'user-agent',
      'screen-resolution',
      'user-IP',
    ]);
    expect(payload[0]['page-id']).to.equal(pageId);
  });
  it('Status code 200 | Should return empty array if no events with such page', async () => {
    const url = `${baseUrl}/35`;
    const response = await server.inject({ url, method, headers: validHeaders });
    expect(response.statusCode).to.equal(200);
    const payload = JSON.parse(response.payload);
    expect(payload).to.be.an.array();
    expect(payload).to.have.length(0);
  });
});
