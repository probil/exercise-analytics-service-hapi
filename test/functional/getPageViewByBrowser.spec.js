'use strict';

/* eslint-disable no-multi-assign */
const {
  describe, it, expect, before,
} = exports.lab = require('lab').script();
const prepareServer = require('../helpers/prepareServer');

let server;
const baseUrl = '/v1/page-view/by-browser';
const method = 'GET';
const validHeaders = { authorization: 'SuperSecretToken' };
const invalidHeaders = { authorization: 'SomethingElse' };

describe('GET /page-view/by-browser', () => {
  before(async () => {
    server = await prepareServer({ forceSync: true, seeders: ['test/fixtures/events.json'] });
  });
  it('Status code 401 | If auth token is missing', async () => {
    const url = `${baseUrl}/Firefox`;
    const response = await server.inject({ url, method });
    expect(response.statusCode).to.equal(401);
  });
  it('Status code 401 | If auth token is wrong', async () => {
    const url = `${baseUrl}/Firefox`;
    const response = await server.inject({ url, method, headers: invalidHeaders });
    expect(response.statusCode).to.equal(401);
  });
  it('Status code 200 | If auth token is right', async () => {
    const url = `${baseUrl}/Firefox`;
    const response = await server.inject({ url, method, headers: validHeaders });
    expect(response.statusCode).to.equal(200);
  });
  it('Status code 200 | Should return array of events with firefox browser', async () => {
    const browser = 'Firefox';
    const url = `${baseUrl}/${browser}`;
    const response = await server.inject({ url, method, headers: validHeaders });
    expect(response.statusCode).to.equal(200);
    const payload = JSON.parse(response.payload);
    expect(payload).to.be.an.array();
    expect(payload).to.have.length(2);
    expect(payload[0]).to.be.an.object();
    expect(payload[0].browser).to.equal(browser);
  });
  it('Status code 200 | Should return array of events with chrome browser', async () => {
    const browser = 'Chrome';
    const url = `${baseUrl}/${browser}`;
    const response = await server.inject({ url, method, headers: validHeaders });
    expect(response.statusCode).to.equal(200);
    const payload = JSON.parse(response.payload);
    expect(payload).to.be.an.array();
    expect(payload).to.have.length(2);
    expect(payload[0]).to.be.an.object();
    expect(payload[0]).to.include([
      'id',
      'timestamp',
      'browser',
      'country',
      'user-id',
      'page-id',
      'page-url',
      'page-referrer',
      'user-agent',
      'screen-resolution',
      'user-IP',
    ]);
    expect(payload[0].browser).to.equal(browser);
  });
  it('Status code 200 | Should return empty array if no events with such browser', async () => {
    const url = `${baseUrl}/Opera`;
    const response = await server.inject({ url, method, headers: validHeaders });
    expect(response.statusCode).to.equal(200);
    const payload = JSON.parse(response.payload);
    expect(payload).to.be.an.array();
    expect(payload).to.have.length(0);
  });
});
