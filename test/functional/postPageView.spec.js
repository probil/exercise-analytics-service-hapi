'use strict';

/* eslint-disable no-multi-assign */
const {
  describe, it, expect, beforeEach,
} = exports.lab = require('lab').script();
const prepareServer = require('../helpers/prepareServer');
const { Event } = require('../../models');

let server;
const baseUrl = '/v1/page-view';
const method = 'POST';
const validHeaders = { authorization: 'SuperSecretToken' };
const invalidHeaders = { authorization: 'SomethingElse' };

describe('POST /page-view', () => {
  beforeEach(async () => {
    server = await prepareServer({ forceSync: true });
  });
  it('Status code 401 | If auth token is missing', async () => {
    const response = await server.inject({ url: baseUrl, method });
    expect(response.statusCode).to.equal(401);
  });
  it('Status code 401 | If auth token is wrong', async () => {
    const response = await server.inject({ method, url: baseUrl, headers: invalidHeaders });
    expect(response.statusCode).to.equal(401);
  });
  it('Status code 400 | If payload is not valid', async () => {
    const payload = {
      'user-id': 'sssss',
    };
    const response = await server.inject({
      method, payload, url: baseUrl, headers: validHeaders,
    });
    expect(response.statusCode).to.equal(400);
  });
  it('Status code 201 | Should create new event if payload is empty', async () => {
    const payload = {};
    const beforeCount = await Event.count();
    expect(beforeCount).to.equal(0);
    const response = await server.inject({
      method, payload, url: baseUrl, headers: validHeaders,
    });
    expect(response.statusCode).to.equal(201);
    const afterCount = await Event.count();
    expect(afterCount).to.equal(1);
  });
  it('Status code 201 | Should create new event if payload contain few fields', async () => {
    const payload = {
      'user-id': 15,
      'screen-resolution': '1280x1024',
    };
    const beforeCount = await Event.count();
    expect(beforeCount).to.equal(0);
    const response = await server.inject({
      method, payload, url: baseUrl, headers: validHeaders,
    });
    expect(response.statusCode).to.equal(201);
    const afterCount = await Event.count();
    expect(afterCount).to.equal(1);
    const addedEvent = await Event.findOne({ where: { 'user-id': 15 } });
    expect(addedEvent.toJSON()).to.part.include(payload);
  });
  it('Status code 201 | Should create new event if payload contain all fields', async () => {
    const payload = {
      timestamp: '1511089276050',
      'user-id': 10,
      'page-id': 19,
      'page-url': 'http://example.com/some-page-19',
      'page-referrer': 'http://example.com/referrer',
      'user-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0',
      'screen-resolution': '800x600',
      'user-IP': '52.41.81.252',
    };
    const beforeCount = await Event.count();
    expect(beforeCount).to.equal(0);
    const response = await server.inject({
      method, payload, url: baseUrl, headers: validHeaders,
    });
    expect(response.statusCode).to.equal(201);
    const afterCount = await Event.count();
    expect(afterCount).to.equal(1);
    const addedEvent = await Event.findOne({ where: { 'user-id': 10 } });

    // we store timestamp in db, and it stores it as date
    // so we should compare it with date later
    const payloadToCompare = Object.assign({}, payload);
    delete payloadToCompare.timestamp;
    expect(addedEvent.toJSON()).to.part.include(payloadToCompare);

    const { timestamp } = addedEvent.toJSON();
    // mysql doesn't store milliseconds so we have to compare
    // values with some delta margin of difference
    expect(+new Date(timestamp)).to.be.about(+payload.timestamp, 100);
  });
  it('Status code 201 | Should create new event and ignore unknown fields', async () => {
    const payload = {
      superField: 15,
      wowow: 25,
      SecretToken: '-----#$$#$',
      'screen-resolution': '800x600',
      'user-id': 10,
      'user-IP': '52.41.81.252',
    };
    const beforeCount = await Event.count();
    expect(beforeCount).to.equal(0);
    const response = await server.inject({
      method, payload, url: baseUrl, headers: validHeaders,
    });
    expect(response.statusCode).to.equal(201);
    const afterCount = await Event.count();
    expect(afterCount).to.equal(1);
    const addedEvent = await Event.findOne({ where: { 'user-id': 10 } });
    const payloadToCompare = Object.assign({}, payload);
    delete payloadToCompare.superField;
    delete payloadToCompare.wowow;
    delete payloadToCompare.SecretToken;
    expect(addedEvent.toJSON()).to.part.include(payloadToCompare);
  });
});
