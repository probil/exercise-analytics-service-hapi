'use strict';

/* eslint-disable no-multi-assign */
const {
  describe, it, expect,
} = exports.lab = require('lab').script();
const sinon = require('sinon');

const registerFilesAsRoutes = require('../../../../helpers/hapi/registerFilesAsRoutes');

describe('/helpers/hapi/registerFilesAsRoutes', () => {
  it('should be High Order Function', () => {
    expect(registerFilesAsRoutes).to.be.a.function();
    expect(registerFilesAsRoutes()).to.be.a.function();
  });
  it('should work if files was not passed', () => {
    const loader = sinon.stub().returns({ method: 'GET' });
    const server = { route: sinon.stub() };
    registerFilesAsRoutes(server, loader, '.')();
    expect(loader.notCalled).to.be.true();
    expect(server.route.notCalled).to.be.true();
  });
  it('should work for single file', () => {
    const loader = sinon.stub().returns({ method: 'GET' });
    const server = { route: sinon.stub() };
    registerFilesAsRoutes(server, loader, '.')('superFile.js');
    expect(loader.calledOnce).to.be.true();
    expect(loader.calledWith('./superFile.js')).to.be.true();
    expect(server.route.calledOnce).to.be.true();
    expect(server.route.calledWith({ method: 'GET' })).to.be.true();
  });
  it('should work for multiple files', () => {
    const loader = sinon.stub().returns({ method: 'GET' });
    const server = { route: sinon.stub() };
    registerFilesAsRoutes(server, loader, '.')(['superFile1.js', 'superFile2.js']);
    expect(loader.calledTwice).to.be.true();
    expect(server.route.withArgs({ method: 'GET' }).calledTwice).to.be.true();
  });
  it('should skip routes that not an object', () => {
    const loader = sinon.stub();
    loader.withArgs('./superFile1.js').returns({ method: 'GET' });
    loader.withArgs('./superFile2.js').returns(undefined);
    const server = { route: sinon.stub() };
    registerFilesAsRoutes(server, loader, '.')(['superFile1.js', 'superFile2.js']);
    expect(loader.calledTwice).to.be.true();
    expect(server.route.calledOnce).to.be.true();
    expect(server.route.calledWith({ method: 'GET' })).to.be.true();
  });
});
