'use strict';

/* eslint-disable no-multi-assign */
const {
  describe, it, expect,
} = exports.lab = require('lab').script();
const cast = require('../../../../helpers/array/cast');

describe('helpers/array/cast', () => {
  it('should return array without modification', () => {
    expect(cast([])).to.equal([]);
    expect(cast([1, 2, 3])).to.equal([1, 2, 3]);
    expect(cast(['test'])).to.equal(['test']);
    expect(cast([null])).to.equal([null]);
  });
  it('should wrap non array to array', () => {
    expect(cast({})).to.equal([{}]);
    expect(cast(1)).to.equal([1]);
    expect(cast('test')).to.equal(['test']);
    expect(cast(null)).to.equal([null]);
  });
});
