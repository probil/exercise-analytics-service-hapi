'use strict';

/* eslint-disable no-multi-assign */
const {
  describe, it, expect,
} = exports.lab = require('lab').script();
const generateModelsMap = require('../../../../helpers/sequelize/generateModelsMap');

describe('helpers/sequelize/generateModelsMap', () => {
  it('should return empty object if nothing passed', () => {
    const expected = {};
    const actual = generateModelsMap();
    expect(expected).to.equal(actual);
  });
  it('should return empty object if empty array passed', () => {
    const expected = {};
    const actual = generateModelsMap([]);
    expect(expected).to.equal(actual);
  });
  it('should correctly generate map for single model', () => {
    const expected = { test: { name: 'test' } };
    const actual = generateModelsMap({ name: 'test' });
    expect(expected).to.equal(actual);
  });
  it('should correctly generate map for multiple model', () => {
    const expected = { test: { name: 'test' }, test3: { name: 'test3' } };
    const actual = generateModelsMap([{ name: 'test' }, { name: 'test3' }]);
    expect(expected).to.equal(actual);
  });
  it('should skip models with no `name` property', () => {
    const expected = { test: { name: 'test' } };
    const actual = generateModelsMap([{ name: 'test' }, { someProp: 'test3' }]);
    expect(expected).to.equal(actual);
  });
});
