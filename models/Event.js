'use strict';

module.exports = (sequelize, DataTypes) => (
  sequelize.define('Event', {
    timestamp: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    browser: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    country: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    'user-id': {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    'page-id': {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    'page-url': {
      type: DataTypes.STRING,
      allowNull: true,
    },
    'page-referrer': {
      type: DataTypes.STRING,
      allowNull: true,
    },
    'user-agent': {
      type: DataTypes.STRING,
      allowNull: true,
    },
    'screen-resolution': {
      type: DataTypes.STRING,
      allowNull: true,
    },
    'user-IP': {
      type: DataTypes.STRING,
      allowNull: true,
    },
  }, {
    tableName: 'events',
    // don't add the timestamp attributes (updatedAt, createdAt)
    timestamps: false,
  })
);
