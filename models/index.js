'use strict';

const Sequelize = require('sequelize');
const config = require('config');
const generateModelsMap = require('../helpers/sequelize/generateModelsMap');

const sequelize = new Sequelize(config.get('database.connection'));
const EventModel = sequelize.import('./Event.js');
const db = generateModelsMap(EventModel);

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
